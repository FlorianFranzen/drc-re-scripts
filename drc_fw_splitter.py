#!/usr/bin/env python
import argparse
from construct import *
import os 
import sys

# construct parser logic
SubFirmware = Struct("subfirmwares",
    ULInt32("offset"),
    ULInt32("length"),
    String("name", 4),
    ULInt32("version"),

    Pointer(lambda ctx: ctx.offset + ctx._.offset,
            OnDemand(Field("data", lambda ctx: ctx.length))),
)

Firmware = Struct("Firmware",
    Anchor("offset"),

    Padding(4, strict=True),
    ULInt32("length"),
    Magic("INDX"),
    Padding(4, strict=True),

    Array(lambda ctx: (ctx.length / 16) - 1, SubFirmware)
)

FlashDump = Struct("FlashDump",
    ULInt32("boot_length"),
    OnDemand(Field("boot_data", lambda ctx: ctx.boot_length)),

    Pointer(lambda ctx: 0xF000, Byte("firmware_to_boot")),

    Sequence("firmwares",
        Optional(Pointer(lambda ctx: 0x0100000, Firmware)),
        Optional(Pointer(lambda ctx: 0x500000, Firmware)),
    ),

    Rename("recovery", Pointer(lambda ctx: 0x1c00000, Firmware))
)

# Helper functions
def fw_get_and_rm_version(fw):
    version = -1
    for (i_subfw, subfw) in enumerate(fw.subfirmwares):
        if(subfw.name == "VER_"):
            version = ULInt32("v").parse(subfw.data.value)
            del fw.subfirmwares[i_subfw]
            break
    return version

# Parse args 
parser = argparse.ArgumentParser(description='Split DRC firmware into the the multiple image used by each processor of the DRC.')

parser.add_argument('firmware', type=argparse.FileType('rb'), help="Firmware file to parse.")

parser.add_argument('-o', '--output-folder', default='.', help='Folder to write the firmware to.')

conf = parser.parse_args()

# Check output folder
conf.output_folder += "/"

if os.path.exists(conf.output_folder):
    if not os.path.isdir(conf.output_folder):
        sys.exit("Can not write to '%s' because it is not a folder." % conf.output_folder)
else:
    os.makedirs(conf.output_folder)

# Parse Firmware
dump = FlashDump.parse_stream(conf.firmware)

print "The following images could be extracted:"

# bootloader
print "    bootloader (%d Byte)" % dump.boot_length
with open(conf.output_folder + "bootloader.bin", "wb") as f:
    f.write(dump.boot_data.value)

# firmware 0 and 1
for (i, firmware) in enumerate(dump.firmwares):
    if firmware is None:
        continue

    version = fw_get_and_rm_version(firmware)

    print "\n    firmware[%d] v%#010x @ %#x%s:" % (i, version, firmware.offset, " (boot)" if i == dump.firmware_to_boot else "")

    for subfirmware in firmware.subfirmwares:
        print "      - {0.name:s} v{0.version:#010x} ({0.length:,d} byte)".format(subfirmware)

        with open(conf.output_folder + "fw-{0.name:s}-{1:#010x}-{0.version:#010x}.bin".format(subfirmware, version), "wb") as f:
            f.write(subfirmware.data.value)


# recovery
version = fw_get_and_rm_version(dump.recovery)

print "\n    recovery v%#010x @ %#x:" % (version, dump.recovery.offset)

for subfirmware in dump.recovery.subfirmwares:
    print "      - {0.name:s} v{0.version:#010x} ({0.length:,d} byte)".format(subfirmware)

    with open(conf.output_folder + "rc-{0.name:s}-{1:#010x}-{0.version:#010x}.bin".format(subfirmware, version), "wb") as f:
        f.write(subfirmware.data.value)

print "\nAll files were write to '%s'." % conf.output_folder

conf.firmware.close()